# Copyright 2023 Abel Deuring
#
# This file is part of brlpr.
#
# brlpr is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
#
# brlpr is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with brlpr. If not, see <https://www.gnu.org/licenses/>.

from enum import Enum
from logging import getLogger
from math import ceil
import packbits
from PIL import Image
from time import sleep, time
import usb.core

from brlpr.errors import BLPError, BLPPrintError, BLPTimeoutError

__all__ = ['P700', 'Errors', 'MediaType', 'TapeColor']

# 04f9:2061: PT-P700. XXX Missing: IDs for PT-H500 and PT-E500.
USB_IDS = ((0x04f9, 0x2061),)

# Printer commands
CMD_COMPRESSION_MODE_TIFF = b'M\x02'
CMD_FORM_FEED = b'\x0c'
CMD_INVALIDATE = b'\x00' * 100
CMD_INITIALIZE = b'\x1b@'
CMD_PRINT_AND_FEED = b'\x1a'
CMD_RASTER_GRAPHICS_TRANSFER = b'G'
CMD_SET_MARGIN = b'\x1bid'
CMD_SET_VARIOUS_MODE_SETTINGS = b'\x1biM'
CMD_STATUS_INFORMATION_REQUEST = b'\x1biS'
CMD_SWITCH_TO_RASTER_MODE = b'\x1Bia\x01'

# Number of bytes expected as status data.
STATUS_LENGTH = 32

# Offsets within the status bytes.
STATUS_ERROR_INFORMATION_1 = 8
STATUS_ERROR_INFORMATION_2 = 9
STATUS_MEDIA_WIDTH = 10
STATUS_MEDIA_TYPE = 11
STATUS_VARIOUS_MODE_SETTINGS = 15
STATUS_REASON = 18
STATUS_PHASE_TYPE = 19
STATUS_PHASE_NUMBER_H = 20
STATUS_PHASE_NUMBER_L = 21
STATUS_NOTIFICATION = 22
STATUS_TAPE_COLOR = 24
STATUS_TEXT_COLOR = 25

# Bits of "various mode settings" byte
VARIOUS_MODE_AUTO_CUT = 0x40
VARIOUS_MODE_MIRROR = 0x80

# The number of "printer dots" that can be used for a given tape width.
# I did not understand the "Raster Command Reference" fully:
# 1. Page 16/17 specifies things like tape width in dots, print
# width in dots, "width offset" in dots.
# 2. Page 36/37 says that uncompressed raster data must be limited
# to some not very well specified number of bytes that is less than 16
# for all tapes with a width < 24mm. Makes sense but: How to calculate
# the concrete offset of the actual print area? OK, the data from page 16/17
# gives some values but here is a quote from page 37:
#
# "The data is expanded by overwriting from the position where the margin
# was added". So... Does this mean that, for a non-zero margin, byte 0 of
# the transferred data is not used for the eight "dots" at one edge of the
# print head but for "dots" within the print area? Would make sense. OTOH,
# the drawing on page 36 suggests that the first byte of the print data is
# always used for the eight "dots" at the edge of the print head.
#
# On the other hand: If TIFF compression is enabled, the content of 16
# uncompressed bytes must be sent for _all_ tape widths, and it seems that
# the actual print area is always "centered" within these 16 bytes. This is
# how "laberlprinterkit" works, and the result looks reasonable when tested
# with the tape widths 6mm, 12mm, 24mm. This indicates that the printer
# knows well how to shift the received data to the appropriate "dots" of the
# print head for the given tape width: 24mm tape needs of course the full
# width of the print head; 12mm tape can't use more than the "lower half"
# of the print head; 6mm tape is cenetered in a cassette with the same
# outer size as a cassette for 12mm tape, and the 6mm tape is centered
# within the cassette. Hence 6mm tape needs the print "dots" that are
# centered within the lower half of the print head.
#
# So this module uses compressed data too, even if it looks a bit silly
# to compress such a small amount of data for a device that isn't that fast.
#
# This also means that the only required parameter is the number of "usable"
# print dot for the given tape type and width. (Heat shrink tube has a
# slightly smaller print size than regular tape.)
#
# MAX_PRINT_HEIGHT_TZE_TAPE: The number of print dots that can be used for
# a given tape width of "regular" TZE tape. The key is a value from
# P700._status[10], as documented in the P700 Raster Command Reference,
# page 27, table 3. The values are taken from column 3 of the table on
# page 14. The same values are listed in the table on page 20.
PRINT_HEIGHT_TZE_TAPE = {
    4: 24,
    6: 32,
    9: 50,
    12: 70,
    18: 112,
    24: 128,
    }

# The number of print dots for 2:1 heat shrink tube. The key value is the
# same as in MAX_PRINT_HEIGHT_TZE_TAPE.
# XXX Some key values are not yet tested and just guessed, since I
# do not have cassettes with all these heat shrink tube widths available.
# And Brother's Raster Command Reference does not explicitly state that
# for example 11.7mm heat shrink tube appears in the status array as the
# value 12.
PRINT_HEIGHT_HS_2_1_TAPE = {
    6: 28,      # 5.8mm
    9: 48,      # 8.8mm
    12: 66,     # 11.7mm
    18: 106,    # 17.7mm
    24: 128,    # 23.6mm
    }

# XXX 3:1 heat shrink tube not yet included. There are sizes where guessing
# the key/status value seems more risky than for 2:1 heat shrink tube: While
# it seems safe to assume that a width of 5.8mm HS 2:1 tube is represented by
# the value 6, the sizes 5.2mm, 11.2mm and 21.0mm of HS 3:1 rube raise the
# question: Are these media types represented in the status data by the
# values 6, 12, 24 (i.e., values that are used for other media types too)
# or do they appear as 5, 11, 21, i.e., values rounded to full mm=
#PRINT_HEIGHT_HS_3_1_TAPE = {
#    }

class Errors(Enum):
    # Values are "bit offsets" into the status bytes 8 and 9
    No_Media = 0
    Cutter_Jam = 2
    Weak_Battery = 3
    # XXX What is this "high voltage adapter"? Just the the wall wart with
    # the 110/220V AC power supply, or somethine else?
    High_Voltage_Adapter = 6
    Replace_Media = 8
    Cover_Open = 12
    Overheating = 13


class MediaType(Enum):
    No_Media = 0
    Laminated_Tape = 1
    Non_Laminated_Tape = 3
    Heat_Shrink_Tube_2_1 = 0x11
    Heat_Shrink_Tube_3_1 = 0x17
    Incompatible = 0xFF
    # Returned if the media byte in the status array has another value than
    # those documented by Brother.
    Unknown = -1


MEDIA_TYPE_BY_NUMBER = {e.value: e for e in MediaType}


class TapeColor(Enum):
    White = 0x01
    Other = 0x02
    Clear = 0x03
    Red = 0x04
    Blue = 0x05
    Yellow = 0x06
    Green = 0x07
    Black = 0x08
    Clear_White_Text = 0x09
    Matte_White = 0x20
    Matte_Clear = 0x21
    Matte_Silver = 0x22
    Satin_Gold = 0x23
    Satin_Silver = 0x24
    Blue_D = 0x30
    Red_D = 0x31
    Fluorescent_Orange = 0x40
    Fluorescent_Yellow = 0x41
    Berry_Pink_S = 0x50
    Light_Gray_S = 0x51
    Lime_Green_S = 0x52
    Yellow_F = 0x60
    Pink_F = 0x61
    Blue_F = 0x62
    White_Heat_Shrink_Tube = 0x70
    White_Flex_ID = 0x90
    Yellow_Flex_ID = 0x91
    # XXX What is "clearning"? (Text copied from the reference manual.)
    # Perhaps some sort of tape und to clean the print head?
    Clearning = 0xF0
    Stencil = 0xF1
    Incompatible = 0xFF
    Unknown = -1


TAPE_COLOR_BY_NUMBER = {e.value: e for e in TapeColor}


class TextColor(Enum):
    White = 0x01
    Red = 0x04
    Blue = 0x05
    Black = 0x08
    Gold = 0x0A
    Blue_F_ = 0x62
    Clearning = 0xF0
    Stencil = 0xF1
    Other = 0x02
    Incompatible = 0xFF
    Unknown = -1


TEXT_COLOR_BY_NUMBER = {e.value: e for e in TextColor}


class StatusReason(Enum):
    """An Enum for byte 18 of the status data, as mentioned on page
    28 of the Raster Command Reference.
    """
    Reply_To_Status_Request = 0
    Printing_Completed = 1
    Error_Occurred = 2
    # Hrm... Do we need Exit_IF_Mode. The reference mentions this
    # value but states that it is not used...
    Exit_IF_Mode = 3
    Turned_Off = 4
    Notification = 5
    Phase_Change = 6


class PrintPhase(Enum):
    """An Enum for the combination of the status bytes 19..21.
    """
    Editing = 0
    Feeding = 1
    Printing = 2
    Cover_Open = 3


class Notification(Enum):
    No_Notification = 0
    Cover_Opened = 1
    Cover_Closed = 2


class PrinterStatus:
    """Translates the 32 status bytes into conveniently usable properties."""
    def __init__(self, raw_data):
        self._raw_data = raw_data

    def __repr__(self):
        return (
            f'<PrinterStatus(reason={self.reason}, '
            f'errors={self.errors}, '
            f'print_phase={self.print_phase}, '
            f'notification={self.notification}, '
            f'media_width={self.media_width}, '
            f'media_type={self.media_type}, '
            f'tape_color={self.tape_color}, '
            f'text_color={self.text_color}, '
            f'_auto_cut={self._auto_cut}, '
            f'_mirror_print={self._mirror_print}'
            f')>')

    @property
    def reason(self):
        """The reason why the printer sent this status data."""
        # XXX This will lead to a ValueError if the P700 provides a value
        # that does mot appear in the PrinterStatis enum. Is this desired
        # behavior or should something like PrinterStatus.Unknown be
        # defined and used in this case?
        return StatusReason(self._raw_data[STATUS_REASON])

    @property
    def errors(self):
        """A tuple of errors indicated in the last status request.

        Members of the returned tuple are items of `class Errors`.
        """
        err_flags = (
            (int(self._raw_data[STATUS_ERROR_INFORMATION_2]) << 8)
            + int(self._raw_data[STATUS_ERROR_INFORMATION_1]))
        return tuple(
            e for e in Errors if err_flags & (1 << e.value))

    @property
    def media_width(self):
        """Width of the currently inserted tape in mm."""
        # Just accept all values, even though table 3 of Brother's Raster
        # Command Reference lists only values for the well known TZe and
        # HS cassettes.
        width = self._raw_data[STATUS_MEDIA_WIDTH]
        # Value 4 needs an obvious adjustment.
        if width == 4:
            return 3.5
        return float(width)

    @property
    def media_type(self):
        """Type of the currently inserted tape.

        The returned value is a member of `class MediaType`.
        """
        if self._raw_data[STATUS_MEDIA_TYPE] in MEDIA_TYPE_BY_NUMBER:
            return MEDIA_TYPE_BY_NUMBER[self._raw_data[STATUS_MEDIA_TYPE]]
        return MediaType.Unknown

    @property
    def print_phase(self):
        """The print phase as reported in bytes 19..21 of the status data.
        """
        # The distinction between phase type and phase number, as documented
        # on page 28 of the Raster Command Reference seems quite cumbersome.
        # Having 3 (three) bytes to encode 4 (value) value looks, well,
        # better no comment...
        # This proberty combines these values into one value of the enum
        # PrintPhase.
        #
        # Yeah, below are lots of magic numbers used. But frankly, I hesitate
        # to define constants just for the sake of putting a bit more
        # clarity into a really strange representation of very few values
        # in three bytes...
        if self._raw_data[STATUS_PHASE_NUMBER_H] == 0:
            # Any other value of STATUS_PHASE_NUMBER_H is undocumented.
            if self._raw_data[STATUS_PHASE_TYPE] == 0:
                if self._raw_data[STATUS_PHASE_NUMBER_L] == 0:
                    return PrintPhase.Editing
                elif self._raw_data[STATUS_PHASE_NUMBER_L] == 1:
                    return PrintPhase.Feeding
                # else: fall through to a exception at the end.
            elif self._raw_data[STATUS_PHASE_TYPE] == 1:
                if self._raw_data[STATUS_PHASE_NUMBER_L] == 0:
                    return PrintPhase.Printing
                elif self._raw_data[STATUS_PHASE_NUMBER_L] == 0x14:
                    return PrintPhase.Cover_Open
        raise BLPError(
            f'Unexpected and undocumented print phase status values: '
            f'{self._raw_data[STATUS_PHASE_TYPE:STATUS_PHASE_NUMBER_L+1]}')

    @property
    def notification(self):
        """The "notification" from the printer in the last readout of the
        status bytes.

        The value indicated if the cover was openend or closed.
        """
        return Notification(self._raw_data[STATUS_NOTIFICATION])

    @property
    def tape_color(self):
        """Tape color.

        The returned value is a member of `class TapeColor`.
        """
        # XXX Not sure if this is a really useful property, at least for the
        # P700:
        # Testing with 3rd-party tapes of different colors, I only see
        # the value "white"... Though this may be due to too simple or "wrong"
        # cassettes provided by the non-Brother tape manufacturer.
        if self._raw_data[STATUS_TAPE_COLOR] in TAPE_COLOR_BY_NUMBER:
            return TAPE_COLOR_BY_NUMBER[self._raw_data[STATUS_TAPE_COLOR]]
        return TapeColor.Unknown

    @property
    def text_color(self):
        """Text color.

        The returned value is a member of `class TextColor`.
        """
        if self._raw_data[STATUS_TEXT_COLOR] in TEXT_COLOR_BY_NUMBER:
            return TEXT_COLOR_BY_NUMBER[self._raw_data[STATUS_TEXT_COLOR]]
        return TextColor.Unknown

    @property
    def _auto_cut(self):
        return bool(
                self._raw_data[STATUS_VARIOUS_MODE_SETTINGS]
                & VARIOUS_MODE_AUTO_CUT)

    @property
    def _mirror_print(self):
        return bool(
            self._raw_data[STATUS_VARIOUS_MODE_SETTINGS]
            & VARIOUS_MODE_MIRROR)


# Print resolution of the P700.
DOTS_PER_INCH = 180
# Minimum and maximum page margins in feeding direction. Specified on
# page 17 of the "Raster Command Reference".
MIN_MARGIN = 14
MAX_MARGIN = 900


logger = getLogger(__name__)


class P700:
    """Driver for a Brother PT-P700.

    According to Brother's "Raster Command Reference", it should also work
    with the models PT-H500 and PT-E500.
    """
    PRINT_DOTS = 128
    PRINT_BYTES = 16
    # Print resolution in tape feeding direction, specified in raster lines
    # per inch.
    RESOLUTION_FEED_DIR = 180
    # The time the printer needs to print one raster line.
    # The speed of the PT-P700 is specified as 30mm/s in data sheets,
    # catalogs etc. So we have (30/25.4) * 180 raster lines per second.
    RASTER_LINE_PRINT_TIME = 25.4 / (30 * RESOLUTION_FEED_DIR)

    def __init__(self, dev=None, serial_number=None):
        def device_matcher(dev):
            return (
                (dev.idVendor, dev.idProduct) in USB_IDS
                and (serial_number is None or dev.serial_number==serial_number))

        if dev is None:
            dev = usb.core.find(custom_match=device_matcher)
            if dev is None:
                raise BLPError('No label printer found.')
        if dev.is_kernel_driver_active(0):
            dev.detach_kernel_driver(0)
        self._dev = dev
        self._margin = MIN_MARGIN
        self.init_device()
        self.read_status()

    def close(self):
        self._dev.finalize()

    def _to_device(self, data, wait_until=None):
        """Write `data` to endpoint 2 of the device.

        `wait_until` is the time as returned by time.time() after which
        a timeout occurs. If this paramter is None, a value of
        `time.time() + 0.1` is used, i.e., a timeout of 0.1 seconds in
        usual terms.
        """
        # Reason for this somewhat convoluted timeout handling:
        # (1) It is possible that not all data is sent in the first write()
        # call. Though I haven't seen this, there is a reason why
        # usb.core.Device.write() returns the number of bytes written.
        # And the method _update_status() definitely needs a loop to read
        # all 32 expected bytes, so I am a bit wary about a similar
        # behaviour with write calls.
        # (2) When a very long label is printed, write() calls
        # with CMD_RASTER_GRAPHICS_TRANSFER and the following raster data
        # need a quite long timeout, up to 15 seconds or so.
        # If the write() call succeeds after a long waiting time but some
        # raster data was not sent, the printer should accept the remaining
        # data much faster. If the first part of the data was sent after,
        # say, 10 seconds, the second write call should not need another
        # 10 seconds.
        # In other words: The timeout parameter of this method is intended for
        # entire data block to be sent, not for single write calls.
        if wait_until is None:
            wait_until = time() + 0.1
        while time() < wait_until and len(data) > 0:
            try:
                timeout = max(int((wait_until - time()) * 1000), 100)
                written = self._dev.write(2, data, timeout=timeout)
                if written is None:
                    # Yeah, the doc of usb.core.Device.write() says that
                    # the return value is the number of bytes written.
                    # But I see the value None instead when
                    # CMD_SWITCH_TO_RASTER_MODE is sent...
                    # No timeout either, and the command seems to have been
                    # successfully sent: The printer accepts
                    # CMD_RASTER_GRAPHICS_TRANSFER. So... just assume that
                    # And the slicing operation `data = data[written:]`
                    # below is effectively a no-op when `written` is None...
                    # None means "no error, everything transmitted".
                    return
                data = data[written:]
            except usb.core.USBTimeoutError:
                raise BLPTimeoutError
        if data:
            raise BLPTimeoutError

    def init_device(self):
        self._to_device(CMD_INVALIDATE)
        self._to_device(CMD_INITIALIZE)

    def _update_status(self, timeout):
        """Try to read the printer status from endpoint 0x81.

        If data is available, self.status is updated and the method returns
        True. Otherwise, the method returns False.

        If `timeout` is zero, the method returns False when the first USB
        read() call does not return any data. Otherwise it waits for `timeout`
        seconds for data from the printer. If the expexted status data is
        not available when the timeout passed, a BLPError is raised.
        """
        data = self._dev.read(0x81, STATUS_LENGTH)
        if not data and timeout == 0:
            return False
        if timeout == 0:
            timeout = 0.1
        wait_until = time() + timeout
        # Sometimes a single read call does not return all expected 32 bytes,
        # hence be a bit patient and try more read calls.
        while len(data) < STATUS_LENGTH and time() < wait_until:
            data += self._dev.read(0x81, STATUS_LENGTH - len(data))
        if len(data) < STATUS_LENGTH:
            raise BLPError(
                f'Timeout reading status data. Received so far: {data}')
        self.status = PrinterStatus(data)
        return True

    def read_status(self, timeout=0.1):
        self._to_device(CMD_STATUS_INFORMATION_REQUEST)
        self._update_status(0.1)

    def _set_various_mode(self, auto_cut, mirror_print):
        new_value = VARIOUS_MODE_AUTO_CUT if auto_cut else 0
        new_value |= VARIOUS_MODE_MIRROR if mirror_print else 0
        new_value = new_value.to_bytes(1, 'big')
        self._to_device(CMD_SET_VARIOUS_MODE_SETTINGS + new_value)

    @property
    def auto_cut(self):
        return self.status._auto_cut

    @auto_cut.setter
    def auto_cut(self, value):
        value = bool(value)
        if value != self.auto_cut:
            self._set_various_mode(value, self.mirror_print)
        self.read_status()

    @property
    def mirror_print(self):
        return self.status._mirror_print

    @mirror_print.setter
    def mirror_print(self, value):
        value = bool(value)
        if value != self.mirror_print:
            self._set_various_mode(self.auto_cut, value)
        self.read_status()

    @property
    def margin(self):
        """Margin in tape feeding direction added before and after the
        print area of a page.

        Specified in print dots.
        """
        return self._margin

    @margin.setter
    def margin(self, new_value):
        new_value = int(new_value)
        if new_value < MIN_MARGIN:
            raise ValueError(
                f'Margin must be greater than or equal to {MIN_MARGIN}')
        if new_value > MAX_MARGIN:
            raise ValueError(
                f'Margin must be less than or equal to {MAX_MARGIN}')
        self._margin = new_value

    @property
    def print_height(self):
        """The possible print height in dots for the currently inserted media.

        The value depends on the media width and the media type.
        Note that a BLPError is raised when 3:1 heat shrink tube is inserted
        into the printer. Unfortunately Brother's Raster Command Reference
        does not document how this type of media is represented in the
        status data provided by the printer to the host computer.
        """
        media_type = self.status.media_type
        if media_type in (
                MediaType.No_Media, MediaType.Incompatible,
                MediaType.Unknown):
            raise BLPError(f'No print height available for {media_type}')
        elif media_type == MediaType.Heat_Shrink_Tube_3_1:
            raise BLPError(
                f"No print height is known for {media_type}.\n\n"
                "Apologies for this interruption.\n"
                "Problem: Brother's Raster Command Reference manual does "
                "not document clearly and unambiguously how the currently "
                "inserted heat shrink tube is represented in the data "
                "provided by the printer to the controlling computer. "
                "If you want to help fixing this problem, send the author "
                "of the brlpr library an email (to: adeuring@gmx.net) with "
                "the following number: "
                f"{self.status._raw_data[STATUS_MEDIA_WIDTH]} "
                "(this is the media width as reported by the printer) "
                "and the width of the heat shrink tube, as declared on the "
                "label of the media cassette.")
        elif media_type == MediaType.Heat_Shrink_Tube_2_1:
            return PRINT_HEIGHT_HS_2_1_TAPE[
                self.status._raw_data[STATUS_MEDIA_WIDTH]]
        else:
            return PRINT_HEIGHT_TZE_TAPE[
                self.status._raw_data[STATUS_MEDIA_WIDTH]]

    def _print_finished(self, last_batch):
        """Send the command `CMD_PRINT_AND_FEED` or `CMD_FORM_FEED`
        to the printer.
        """
        if last_batch:
            self._to_device(CMD_PRINT_AND_FEED)
        else:
            self._to_device(CMD_FORM_FEED)

    def _check_status(self):
        """Helper for print_images(): Check if a status message from the
        printer indicates an error. If so, raise an exception.
        """
        if self._update_status(0):
            if self.status.reason == StatusReason.Error_Occurred:
                # This is too simple: The "cover open" error can perhaps be
                # easily fixed. In that case it can make sense
                # to optionally invoke some kind of callback
                # so that the UI can notify the user and to wait
                # that the cover is closed again.

                error_status = self.status
                # We may get more than one status message for the very same
                # error... For now, just check for a short time if more
                # status messages arrive and stuff everything into the
                # exception.
                wait_until = time() + 0.1
                more_statuses = []
                while time() < wait_until:
                    if self._update_status(0):
                        more_statuses.append(self.status)
                raise BLPPrintError(
                    f'Error while printing: {error_status}',
                    error_status, more_statuses)
            return True
        return False

    def _print_bytes_from_int(self, raster_line, shift, mask):
        """Convert an integer `raster_line` that contains bit values for
        a raster line into a bytes object with a "TIFF compressed"
        representation of the raster data.
        """
        raster_line = raster_line & mask
        raster_line = raster_line << shift
        compressed = packbits.encode(
            raster_line.to_bytes(self.PRINT_BYTES, 'big'))
        return len(compressed).to_bytes(2, 'little') + compressed

    def print_raster_line_ints(self, raster_line_ints, last_batch=True):
        """Print a sequence of raster lines.

        A low level print method.

        `raster_lines` is a sequence or an iterator of raster line data
        where each element is an integer that represents the data of one
        raster line perpendicular to the tape feeding direction.

        Each bit of these integers indicates if a certain pin of the print
        head is activated (bit value 1) or not activated (bit value 0).

        Position of the bits on the tape (mirror-print disabled):

        <------- tape feeding direction
        MSB
        ...
        LSB
        <------- tape feeding direction

        MSB: Most significant bit (as an integer value: 1 << (print_height-1))
        LSB: Least significant bit (as an integer value: 1)

        Any integer value may appear in `raster_line_ints` but only the
        lowest `self.print_height` bits are used for printing.

        If `last_batch` is False, the command `CMD_FORM_FEED` is sent
        to the printer after all raster data has been sent.

        If `last_batch` is True, the command `CMD_PRINT_AND_FEED` is sent
        to the printer after all raster data has been sent.

        The behaviour of the printer depends on `last_batch` and on the
        property `auto_cut`.

        1. `auto_cut` is False:

        `CMD_FORM_FEED` lets the printer advance the tape by an amount
        that is determied by the property `margin`. The tape is not cut.

        `CMD_PRINT_AND_FEED` lets the printer advance the tape so that
        it is cut after the last printed "row" plus a margin that is
        determined by the property `margin`.

        Before the print is a margin of ca 22mm.

        2. `auto_cut` is True:

        The tape is advanced by ca 25mm and cut. Nothing is printed on
        that piece of tape. The print appears on the tape after the cut
        plus a margin determined by the property `margin`. When the print
        is completed, the tape is advanced by ca 22mm and cut, regardless
        if `last_batch` is Ture or False.

        XXX missing: Explanation how the the "advanced mode settings"
        affect the activation of the cutter.
        """
        # Prepare printing:
        # Ensure a clean state of the printer and update the status data:
        # The user may have changed the media after the last call of
        # read_status() so that the tape width may have changed. But
        # correct parameters are essential.
        self.init_device()
        self.read_status()
        # Ensure that "bad values" in the print data sequence do not lead
        # to the activation of print head "dots" outside the current media
        # width.
        input_mask = 2 ** self.print_height - 1
        center_shift = (self.PRINT_DOTS - self.print_height) // 2

        self._to_device(CMD_SWITCH_TO_RASTER_MODE)
        self._to_device(CMD_COMPRESSION_MODE_TIFF)
        self._to_device(CMD_SET_MARGIN + self._margin.to_bytes(2, 'little'))
        # XXX missing: set "advanced mode".
        print_started = time()
        line_count = 0
        for raster_line_int in raster_line_ints:
            if self._check_status():
                if self.status.reason == StatusReason.Notification:
                    # XXX This is kinda theoretical code... While the
                    # Raster Command Reference suggests on page 29 that
                    # the P700 might send a notification when the
                    # cover is opened or closed,  I haven't seen such
                    # a status message yet. So, just something for
                    # the logger.
                    logger.info(f'Received a notification {self.status}')
                elif self.status.reason == StatusReason.Phase_Change:
                    logger.debug(f'Phase change {self.status}')
                elif self.status.reason == StatusReason.Error_Occurred:
                    raise BLPPrintError(
                        f'Error while printing: {self.status}',
                        self.status)
                else:
                    raise BLPPrintError(
                        'Unexpected status message: {self.status}',
                        self.status)

            print_data = self._print_bytes_from_int(
                raster_line_int, center_shift, input_mask)
            # XXX The timing calculation below assumes that raster data
            # is produced faster than the printer can consume it. True
            # in usual situations but there _may_ be a very slow image
            # iterator. In that case, wait_until could very well be less
            # than time() and the entire logic breaks.
            #
            # self.RASTER_LINE_PRINT_TIME * line_count is the expected
            # print time based on the asumption that 30mm tape are
            # printed per second. There seems to be an additional start
            # delay of about 1.5 seconds or so. Add 5 seconds to the
            # tineout to be on the safe side.
            wait_until = (
                print_started + self.RASTER_LINE_PRINT_TIME * line_count
                + 5)
            line_count += 1
            self._to_device(
                CMD_RASTER_GRAPHICS_TRANSFER + print_data, wait_until)
        self._print_finished(last_batch)
        while True:
            # XXX 30 seconds are quite arbitrary as the time to wait until
            # printing is finished.
            wait_until = time() + 30
            if self._check_status():
                # A simple check self.status.print_phase == PrintPhase.Editing
                # is not enough: When a small label is printed, the printer
                # is still in PrintPhase.Editing; the switch to to the
                # phase "printing" will be seen here. But when the
                # printer has received the command CMD_FORM_FEED or
                # CMD_PRINT_AND_FEED it will eventually send the message
                # as expected in the "if" expression.
                if (
                        self.status.reason == StatusReason.Phase_Change
                        and self.status.print_phase == PrintPhase.Editing):
                    break
            sleep(0.1)

    def _pil_raster_lines(self, image):
        """Generate raster line data as needed by `print_raster_line_ints()`
        from a PIL image. The image must be in mode '1', its height
        must not exceed `self.print_height`.

        Images with a height smaller than `self.print_height` are centered.
        """
        if image.size[1] > self.print_height:
            raise ValueError(
                f'The image height must less than or equal to '
                f'{self.print_height} for the current print media.')
        if image.mode != '1':
            raise ValueError('Only bitmap images (mode 1) can be printed.')
        image = image.transpose(
            Image.Transpose.ROTATE_90).transpose(
                Image.Transpose.FLIP_TOP_BOTTOM)
        bytes_per_line, remain = divmod(image.size[0], 8)
        if remain:
            bytes_per_line += 1
            padding_bits = 8 - remain
        else:
            padding_bits = 0
        center_shift = (self.print_height - image.size[0]) // 2
        line_mask = 2 ** image.size[0] - 1
        bitmap = image.tobytes()
        for offset in range(0, len(bitmap), bytes_per_line):
            line = int.from_bytes(bitmap[offset:offset+bytes_per_line], 'big')
            line = line >> padding_bits
            # We need the bit value 1 for pixels that appear black in the
            # print (assuming the most common media type where black text/
            # images are printed on some background/media color that is not
            # black), while image.tobytes() uses "inverted" values: 0 is
            # black and 1 is white.
            line = ~line & line_mask
            line = line << center_shift
            yield line

    def _pil_images_raster_line_iterator(self, images):
        """Loop over the provided `images` and yield a raster line integer
        for each "vertical line" of each image.
        """
        for im in images:
            for line in self._pil_raster_lines(im):
                yield line

    def print_images(self, images, last_batch=True):
        """Print a sequence of PIL images.

        `images` is a sequence or an iterator of PIL images that must
        match these constraints:

        - mode is '1', i.e. one bit per pixel
        - max height must be less than or equal to `self.print_height`.
          The value of this property depends on the media width and type.

        A pixel value of 0 means "black" in the sense that a dot of the print
        head is activated for this value.

        The horizontal axis of the images is parallel to the tape feeding
        direction; the leftmost "pixel column" is printed first.

        All images are printed without any margin between them. The cutter is
        never activated between two images.

        If `last_batch` is True, the command `CMD_PRINT_AND_FEED` is sent
        to the printer after the last image. The tape is advanced so that
        it can be cut at the right edge of the margin (defined by the
        property `margin`) that is added by the printer after the last
        printed row.

        If `last_batch` is False, the command `CMD_FORM_FEED` is sent
        to the printer. XXX Missing: What that means in detail.

        XXX missing: Explanation how the property `auto_cut`, the "advanced
        mode settings" and `last_batch` affect the activation of the cutter.
        """
        # Ensure that the print height is right.
        self.print_raster_line_ints(
            self._pil_images_raster_line_iterator(images), last_batch)
