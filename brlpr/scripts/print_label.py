# Copyright 2023 Abel Deuring
#
# This file is part of brlpr.
#
# brlpr is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
#
# brlpr is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with brlpr. If not, see <https://www.gnu.org/licenses/>.

"""A simple script to print labels with PT-P700 from the shell."""


from argparse import ArgumentParser
from PIL import Image, ImageDraw, ImageFont
import sys

from brlpr.printer.p700 import P700


def render_text(font_name, height, text):
    """Render `text` into a PIL image with the given `height`.

    `font_name` is the name of a truetype font.

    Known bug: For texts with more than one line, the descender part of
    the last line (i.e., the part of characters like 'q' or 'y' below the
    base line) is cut off.
    """
    if text is None:
        text = font_name
    im = Image.new('1', (1000, height), color=1)
    draw = ImageDraw.Draw(im)
    font_size = 1
    text_height = 0
    while text_height < height:
        font = ImageFont.truetype(font_name, font_size)
        bbox = draw.multiline_textbbox((0, 0), text, font=font)
        text_height = bbox[3]
        font_size += 1
    font_size -= 1
    font = ImageFont.truetype(font_name, font_size)
    bbox = draw.multiline_textbbox((0, 0), text, font=font)
    im = Image.new('1', (bbox[2], height), color=1)
    draw = ImageDraw.Draw(im)
    draw.multiline_text((0, 0), text, font=font)
    return im


def image_iterator(args, printer):
    if args.margin < 0:
        print('Margin cannot be negative.', file=sys.stderr)
        sys.exit(-1)
    margin_pixels = int(args.margin * printer.RESOLUTION_FEED_DIR / 25.4)
    if margin_pixels > 0:
        margin_image = Image.new('1', (margin_pixels, 1), color=1)
    else:
        margin_image = None
    if args.separator:
        separator_image = render_text(
            args.font, printer.print_height, args.separator)
    else:
        separator_image = None

    if separator_image is not None:
        yield separator_image
    for text in args.text:
        if margin_image is not None:
            yield margin_image
        yield render_text(args.font, printer.print_height, text)
        if margin_image is not None:
            yield margin_image
        if separator_image is not None:
            yield separator_image


def get_args(args):
    p = ArgumentParser()
    p.add_argument(
        '-f', '--font', required=True,
        help='Name of the truetype font used to render the text.')
    p.add_argument(
        '-s', '--separator', default='',
        help='A symbol like "|" that is inserted between two printed texts')
    p.add_argument(
        '-m', '--margin', default=1.0, type=float,
        help='The margin in millimeter added before and after a label')
    p.add_argument(
        'text', nargs='+',
        help='Label text to print. It may include line separators.')
    return p.parse_args(args)


def main():
    args = get_args(sys.argv[1:])
    printer = P700()
    printer.print_images(image_iterator(args, printer))

if __name__ == '__main__':
    main()
