# Copyright 2023 Abel Deuring
#
# This file is part of brlpr.
#
# brlpr is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
#
# brlpr is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with brlpr. If not, see <https://www.gnu.org/licenses/>.

from array import array
from collections import namedtuple
from contextlib import contextmanager
import os
import packbits
from PIL import Image, ImageDraw
from unittest import skipUnless, TestCase
import usb.core

from brlpr.errors import BLPError, BLPPrintError
from brlpr.printer.p700 import (
    CMD_COMPRESSION_MODE_TIFF,
    CMD_INVALIDATE,
    CMD_INITIALIZE,
    CMD_PRINT_AND_FEED,
    CMD_RASTER_GRAPHICS_TRANSFER,
    CMD_SET_MARGIN,
    CMD_STATUS_INFORMATION_REQUEST,
    CMD_SWITCH_TO_RASTER_MODE,
    Errors, Notification, MediaType, P700, PrintPhase, PrinterStatus,
    STATUS_ERROR_INFORMATION_1, STATUS_ERROR_INFORMATION_2,
    STATUS_MEDIA_TYPE,
    STATUS_MEDIA_WIDTH, STATUS_NOTIFICATION,
    STATUS_PHASE_NUMBER_H, STATUS_PHASE_NUMBER_L,
    STATUS_PHASE_TYPE,
    STATUS_REASON, STATUS_TAPE_COLOR, STATUS_TEXT_COLOR, StatusReason,
    TAPE_COLOR_BY_NUMBER,
    TapeColor,
    TEXT_COLOR_BY_NUMBER, TextColor, USB_IDS,
    )


class TestPrinterStatus(TestCase):
    """Tests for class PritnerStatus.

    Mostly "synthetic" tests where no real status data is retrieved from
    a P700 printer.
    """
    # Some reasonabel default values for the statis data. Mostly copied
    # from page 24 of the Raster Command Reference.
    DEFAULT_STATUS_DATA = (
        0x80,   # print head mark
        0x20,   # size
        0x42,   # "Brother code"
        0x30,   # series code
        0x67,   # model code
        0x30,   # country code
        0,      # reserved
        0,      # reserved
        0,      # error information 1
        0,      # error information 2
        12,     # media width
        1,      # media tape (1 -> laminated tape)
        0,      # nmber of colors
        0,      # fonts
        0,      # Japanese fonts
        0,      # mode or "various mode settings"
        0,      # density
        0,      # media length
        0,      # status type in Brother's jargon, "status reason" in this
                # library
        0,      # phase type
        0,      # phase number, H
        0,      # phase number, L
        0,      # notification number
        0,      # expansion area
        1,      # tape color (1 -> white)
        8,      # text coloe (8 -> black)
        0,      # hardware settings
        0,      # reserved
        0,      # reserved
        )

    def make_status_data(self, modifier={}):
        result = array('B', self.DEFAULT_STATUS_DATA)
        for k, v in modifier.items():
            result[k] = v
        return result

    def test_reason(self):
        for reason in range(7):
            status = PrinterStatus(
                self.make_status_data({STATUS_REASON: reason}))
            self.assertEqual(
                StatusReason(reason), status.reason)
        # Other values for reason lead to an error on attempts to access the
        # "reason" property.
        with self.assertRaises(ValueError):
            PrinterStatus(
                self.make_status_data({STATUS_REASON: 7})).reason

    def test_errors(self):
        status = PrinterStatus(
            self.make_status_data())
        self.assertEqual((), status.errors)

        status = PrinterStatus(
            self.make_status_data({STATUS_ERROR_INFORMATION_1: 1}))
        self.assertEqual((Errors.No_Media, ), status.errors)

        status = PrinterStatus(
            self.make_status_data({STATUS_ERROR_INFORMATION_1: 5}))
        self.assertEqual((Errors.No_Media, Errors.Cutter_Jam), status.errors)

        status = PrinterStatus(
            self.make_status_data({STATUS_ERROR_INFORMATION_1: 0x40}))
        self.assertEqual(
            (Errors.High_Voltage_Adapter, ), status.errors)

        status = PrinterStatus(
            self.make_status_data({
                STATUS_ERROR_INFORMATION_1: 0x40,
                STATUS_ERROR_INFORMATION_2: 0x01}))
        self.assertEqual(
            (Errors.High_Voltage_Adapter, Errors.Replace_Media), status.errors)

        status = PrinterStatus(
            self.make_status_data({STATUS_ERROR_INFORMATION_2: 0x10}))
        self.assertEqual((Errors.Cover_Open, ), status.errors)

        status = PrinterStatus(
            self.make_status_data({STATUS_ERROR_INFORMATION_2: 0x20}))
        self.assertEqual((Errors.Overheating, ), status.errors)

        # All other bits in status[8:9] are ignored.
        status = PrinterStatus(
            self.make_status_data({
                STATUS_ERROR_INFORMATION_1: 0xb2,
                STATUS_ERROR_INFORMATION_2: 0xce}))
        self.assertEqual((), status.errors)

    def test_media_width(self):
        for value in range(256):
            status = PrinterStatus(
            self.make_status_data({STATUS_MEDIA_WIDTH: value}))
            if value != 4:
                self.assertEqual(value, status.media_width)
            else:
                self.assertEqual(3.5, status.media_width)
            self.assertTrue(isinstance(status.media_width, float))

    def test_media_type(self):
        type_map = {
            0: MediaType.No_Media,
            1: MediaType.Laminated_Tape,
            3: MediaType.Non_Laminated_Tape,
            0x11: MediaType.Heat_Shrink_Tube_2_1,
            0x17: MediaType.Heat_Shrink_Tube_3_1,
            0Xff: MediaType.Incompatible,
            }
        for raw_value in range(256):
            status = PrinterStatus(
                self.make_status_data({STATUS_MEDIA_TYPE: raw_value}))
            if raw_value in type_map:
                self.assertEqual(type_map[raw_value], status.media_type)
            else:
                self.assertEqual(MediaType.Unknown, status.media_type)

    def test_print_phase(self):
        # Map (status_byte_19, status_byte_21) to the expected Enum value.
        expected = {
            (0, 0): PrintPhase.Editing,
            (0, 1): PrintPhase.Feeding,
            (1, 0): PrintPhase.Printing,
            (1, 20): PrintPhase.Cover_Open,
            }
        for setting, expected_phase in expected.items():
            status = PrinterStatus(
                self.make_status_data({
                    STATUS_PHASE_TYPE: setting[0],
                    STATUS_PHASE_NUMBER_H: 0,
                    STATUS_PHASE_NUMBER_L: setting[1]}))
            self.assertEqual(expected_phase, status     .print_phase)
        # Selection of invalid/undetected print phase values:
        with self.assertRaises(BLPError):
            PrinterStatus(
                self.make_status_data({
                    STATUS_PHASE_TYPE: 2,
                    STATUS_PHASE_NUMBER_H: 0,
                    STATUS_PHASE_NUMBER_L: 0})).print_phase
        with self.assertRaises(BLPError):
            PrinterStatus(
                self.make_status_data({
                    STATUS_PHASE_TYPE: 0,
                    STATUS_PHASE_NUMBER_H: 1,
                    STATUS_PHASE_NUMBER_L: 0})).print_phase
        with self.assertRaises(BLPError):
            PrinterStatus(
                self.make_status_data({
                    STATUS_PHASE_TYPE: 0,
                    STATUS_PHASE_NUMBER_H: 0,
                    STATUS_PHASE_NUMBER_L: 100})).print_phase
        with self.assertRaises(BLPError):
            PrinterStatus(
                self.make_status_data({
                    STATUS_PHASE_TYPE: 1,
                    STATUS_PHASE_NUMBER_H: 0,
                    STATUS_PHASE_NUMBER_L: 1})).print_phase

    def test_notification(self):
        status = PrinterStatus(
            self.make_status_data({STATUS_NOTIFICATION: 0}))
        self.assertEqual(Notification.No_Notification, status.notification)
        status = PrinterStatus(
            self.make_status_data({STATUS_NOTIFICATION: 1}))
        self.assertEqual(Notification.Cover_Opened, status.notification)
        status = PrinterStatus(
            self.make_status_data({STATUS_NOTIFICATION: 2}))
        self.assertEqual(Notification.Cover_Closed, status.notification)

        for value in range(3, 256):
            status = PrinterStatus(
                self.make_status_data({STATUS_NOTIFICATION: value}))
            with self.assertRaises(ValueError):
                status.notification

    def test_tape_color(self):
        # Hrm... no, I won't type again the mapping just for this test.
        # number -> enum value. Just check that rhe property indeed
        # accesses the dictionary TAPE_COLOR_BY_NUMBER and that the
        # returned values have the expected type.
        for raw_value in range(256):
            status = PrinterStatus(
                self.make_status_data({STATUS_TAPE_COLOR: raw_value}))
            tape_color = status.tape_color
            if raw_value in TAPE_COLOR_BY_NUMBER:
                self.assertEqual(
                    TAPE_COLOR_BY_NUMBER[raw_value], tape_color)
            else:
                self.assertEqual(TapeColor.Unknown, tape_color)
            self.assertTrue(isinstance(tape_color, TapeColor))

    def test_text_color(self):
        for raw_value in range(256):
            status = PrinterStatus(
                self.make_status_data({STATUS_TEXT_COLOR: raw_value}))
            text_color = status.text_color
            if raw_value in TEXT_COLOR_BY_NUMBER:
                self.assertEqual(
                    TEXT_COLOR_BY_NUMBER[raw_value], text_color)
            else:
                self.assertEqual(TextColor.Unknown, text_color)
            self.assertTrue(isinstance(text_color, TextColor))


class DeviceLogger:
    READ_DATA = 0
    WRITE_DATA = 1
    LogEntry = namedtuple('LogEntry', ('type_', 'args', 'kwargs', 'result'))

    def __init__(self, usb_device):
        self.usb_device = usb_device
        self.log = []

    def read(self, *args, **kwargs):
        result = self.real_read(*args, **kwargs)
        # Ignore "empty" read calls: There may be thousands of them.
        if result:
            self.log.append(self.LogEntry(self.READ_DATA, args, kwargs, result))
        return result

    def write(self, *args, **kwargs):
        self.log.append(self.LogEntry(self.WRITE_DATA, args, kwargs, None))
        self.real_write(*args, **kwargs)

    @classmethod
    @contextmanager
    def patch(cls, usb_device):
        logger = cls(usb_device)
        logger.real_write = usb_device.write
        logger.real_read = usb_device.read
        usb_device.write = logger.write
        usb_device.read = logger.read
        try:
            yield logger
        finally:
            usb_device.write = logger.real_write
            usb_device.read = logger.real_read

    @property
    def read_log_entries(self):
        return (e for e in enumerate(self.log) if e[1].type_ == self.READ_DATA)

    @property
    def write_log_entries(self):
        return (e for e in enumerate(self.log) if e[1].type_ == self.WRITE_DATA)


class TestP700(TestCase):
    """Tests for class P700.

    Obvious condition for the tests to succeed: At least one supported
    printer must be connected to the test machine.
    """
    def check_init(self, printer):
        # Common checks for successful calls of P700.__init__()
        self.assertTrue(isinstance(printer._dev, usb.core.Device))
        self.assertIn(
            (printer._dev.idVendor, printer._dev.idProduct), USB_IDS)
        self.assertFalse(printer._dev.is_kernel_driver_active(0))
        # The printer's status data was retrieved. Just a check that the
        # attribute with the raw data exists.
        printer.status.reason

    @contextmanager
    def get_printer(self, dev=None, serial_number=None):
        # Create and return a P700 instance. Finally ensure that the close()
        # method is called.
        printer = P700()
        try:
            yield printer
        finally:
            printer.close()

    def test_init_simple(self):
        # No parameter is required. In  this case the constructor searches
        # for any USB device with known vendor and product IDs.
        with self.get_printer() as printer:
            self.check_init(printer)

    def test_init_with_usb_device(self):
        # An instance of class usb.core.Device can be passed to the constructor.
        for vendor, product in USB_IDS:
            dev = usb.core.find(idVendor=vendor, idProduct=product)
            if dev is not None:
                break
        with self.get_printer(dev) as printer:
            self.check_init(printer)

    def test_init_with_serial_number(self):
        # An instance of class usb.core.Device can be passed to the constructor.
        for vendor, product in USB_IDS:
            dev = usb.core.find(idVendor=vendor, idProduct=product)
            if dev is not None:
                break
        serial_number = dev.serial_number
        dev.finalize()
        with self.get_printer(serial_number=serial_number) as printer:
            self.assertEqual(serial_number, printer._dev.serial_number)
            self.check_init(printer)

    def test_auto_cut(self):
        with self.get_printer() as printer:
            # auto_cut is the value of bit 6 of byte 15 of the status array.
            printer.status._raw_data[15] = 0
            self.assertFalse(printer.auto_cut)
            printer.status._raw_data[15] = 0x40
            self.assertTrue(printer.auto_cut)

            # Applications can change the settings vie the
            # property setter.
            # For this test, we must re-read the status data since it was
            # perhaps "illegally changed" by the lines above.
            printer.read_status()

            current_value = printer.auto_cut
            printer.auto_cut = not printer.auto_cut

            self.assertEqual(not current_value, printer.auto_cut)

    def test_mirror_print(self):
        with self.get_printer() as printer:
            # mirror_print is the value of bit 7 of byte 15 of the status array,
            printer.status._raw_data[15] = 0
            self.assertFalse(printer.mirror_print)
            printer.status._raw_data[15] = 0x80
            self.assertTrue(printer.mirror_print)

            # Applications can (and should) cjange the settings vie the
            # property setter.
            # For this test, we must re-read the status data since it was
            # perhaps "illegally changed" by the lines above.
            printer.read_status()

            current_value = printer.mirror_print
            printer.mirror_print = not printer.mirror_print

            self.assertEqual(not current_value, printer.mirror_print)

    def test_margin(self):
        with self.get_printer() as printer:
            printer.margin = 42
            self.assertEqual(42, printer.margin)
            printer.margin = 43
            self.assertEqual(43, printer.margin)
            # Floats are converted to ints.
            printer.margin = 123.4
            self.assertEqual(123, printer.margin)
            # Anything that can't be converted to an int raises a ValueError.
            with self.assertRaises(ValueError):
                printer.margin = 'nonsense'
            # Setting a value less than MIN_MARGIN (15) or greater than
            # MAX_MARGIN raises a ValueError.
            printer.margin = 14
            with self.assertRaises(ValueError) as ctx:
                printer.margin = 13
            self.assertEqual(
                ('Margin must be greater than or equal to 14',),
                ctx.exception.args)
            printer.margin = 900
            with self.assertRaises(ValueError) as ctx:
                printer.margin = 901
            self.assertEqual(
                ('Margin must be less than or equal to 900',),
                ctx.exception.args)

    def test_print_height(self):
        # Values from page 20 of the P700 Raster Command Reference.
        expected_print_height_regular = {
            4: 24, 6: 32, 9: 50, 12: 70, 18: 112, 24: 128,
            }
        # Values from page 21 of the P700 Raster Command Reference.
        expected_print_height_hs_2_1 = {
            6: 28, 9: 48, 12: 66, 18: 106, 24: 128,
            }
        with self.get_printer() as printer:
            for media_type in (
                    MediaType.Laminated_Tape, MediaType.Laminated_Tape):
                printer.status._raw_data[STATUS_MEDIA_TYPE] = (
                    media_type.value)
                for media_width in (4, 6, 9, 12, 18, 24):
                    printer.status._raw_data[STATUS_MEDIA_WIDTH] = (
                        media_width)
                    self.assertEqual(
                        expected_print_height_regular[media_width],
                        printer.print_height)

            printer.status._raw_data[STATUS_MEDIA_TYPE] = (
                MediaType.Heat_Shrink_Tube_2_1.value)
            for media_width in (6, 9, 12, 18, 24):
                printer.status._raw_data[STATUS_MEDIA_WIDTH] = media_width
                self.assertEqual(
                    expected_print_height_hs_2_1[media_width],
                        printer.print_height)

            printer.status._raw_data[STATUS_MEDIA_TYPE] = (
                MediaType.Heat_Shrink_Tube_3_1.value)
            with self.assertRaises(BLPError) as ctx:
                printer.print_height
            expected = (
                "No print height is known for MediaType.Heat_Shrink_Tube_3_1."
                "\n\n"
                "Apologies for this interruption.\n"
                "Problem: Brother's Raster Command Reference manual does "
                "not document clearly and unambiguously how the currently "
                "inserted heat shrink tube is represented in the data "
                "provided by the printer to the controlling computer. If "
                "you want to help fixing this problem, send the author of "
                "the brlpr library an email (to: adeuring@gmx.net) with "
                "the following number: 24 (this is the media width as "
                "reported by the printer) and the width of the heat shrink "
                "tube, as declared on the label of the media cassette.")
            self.assertEqual(expected, ctx.exception.args[0])

            # 128 in the loop values below: Example of a value that does not
            # appear in the enum MediaType.
            for media_type in (
                    MediaType.No_Media, MediaType.Incompatible, 128):
                if isinstance(media_type, MediaType):
                    mtype_num = media_type.value
                else:
                    mtype_num = media_type
                printer.status._raw_data[STATUS_MEDIA_TYPE] = mtype_num
                with self.assertRaises(BLPError) as ctx:
                    printer.print_height
                if isinstance(media_type, MediaType):
                    m_type_for_err_msg = media_type
                else:
                    m_type_for_err_msg = MediaType.Unknown
                expected = f'No print height available for {m_type_for_err_msg}'
                self.assertEqual(expected, ctx.exception.args[0])

    def _check_print_line(self, printer, value, expected):
        mask = 2 ** printer.print_height - 1
        shift = (128 - printer.print_height) // 2
        result = printer._print_bytes_from_int(value, shift, mask)
        packed_len = int.from_bytes(result[:2], 'little')
        self.assertEqual(packed_len + 2, len(result))
        unpacked = packbits.decode(result[2:])
        self.assertEqual(expected, unpacked)

    def test_print_bytes_from_int__3_5mm_tape(self):
        # 24 print head "dots" used. 52 "dots" at the top/bottom are not used.
        with self.get_printer() as printer:
            # monkey patch the tape width.
            printer.status._raw_data[STATUS_MEDIA_WIDTH] = 4
            mask = 2 ** printer.print_height - 1
            shift = (128 - printer.print_height) // 2

            # All zero bits.
            self._check_print_line(printer, 0, b'\x00' * 16)
            # All one bits.
            self._check_print_line(
                printer, -1,
                b'\x00' * 6 + b'\x0f' + b'\xff\xff' + b'\xf0' + b'\x00' * 6)
            # Only "leftmost" bit set.
            self._check_print_line(
                printer, 1, b'\x00' * 9 + b'\x10' + b'\x00' * 6)
            # Largest useful bit set.
            self._check_print_line(
                printer, 1 << (printer.print_height-1),
                b'\x00' * 6 + b'\x08' + b'\x00' * 9)
            # Value is too big.
            self._check_print_line(
                printer, 1 << (printer.print_height), b'\x00' * 16)
            # Central two bits.
            self._check_print_line(
                printer, 3 << (printer.print_height//2-1),
                b'\x00' * 7 + b'\x01\x80' + b'\x00' * 7)

    def test_print_bytes_from_int__12mm_tape(self):
        # 70 print head "dots" used. 29 "dots" at the top/bottom are not used.
        with self.get_printer() as printer:
            # monkey patch the tape width.
            printer.status._raw_data[STATUS_MEDIA_WIDTH] = 12
            mask = 2 ** printer.print_height - 1
            shift = (128 - printer.print_height) // 2

            # All zero bits.
            self._check_print_line(printer, 0, b'\x00' * 16)
            # All one bits.
            self._check_print_line(
                printer, -1,
                b'\x00' * 3 + b'\x07' + b'\xff' * 8 + b'\xe0' + b'\x00' * 3)
            # Only "leftmost" bit set.
            self._check_print_line(
                printer, 1, b'\x00' * 12 + b'\x20' + b'\x00' * 3)
            # Largest useful bit set.
            self._check_print_line(
                printer, 1 << (printer.print_height-1),
                b'\x00' * 3 + b'\x04' + b'\x00' * 12)
            # Value is too big.
            self._check_print_line(
                printer, 1 << (printer.print_height), b'\x00' * 16)
            # Central two bits.
            self._check_print_line(
                printer, 3 << (printer.print_height//2-1),
                b'\x00' * 7 + b'\x01\x80' + b'\x00' * 7)

    def test_print_bytes_from_int__18mm_tape(self):
        # 112 print head "dots" used.
        with self.get_printer() as printer:
            # monkey patch the tape width.
            printer.status._raw_data[STATUS_MEDIA_WIDTH] = 18
            mask = 2 ** printer.print_height - 1
            shift = (128 - printer.print_height) // 2

            # All zero bits.
            self._check_print_line(printer, 0, b'\x00' * 16)
            # All one bits.
            # With 18mm tape, 112 print head "dots" can be used, eight "dots"
            # at the top and botom are set to zero.
            self._check_print_line(
                printer, -1, b'\x00' + b'\xff' * 14 + b'\x00')
            # Only "leftmost" bit set.
            self._check_print_line(printer, 1, b'\x00' * 14 + b'\x01' + b'\x00')
            # Largest useful bit set.
            self._check_print_line(
                printer, 1 << (printer.print_height-1),
                b'\x00\x80' + b'\x00' * 14)
            # Value is too big.
            self._check_print_line(
                printer, 1 << (printer.print_height), b'\x00' * 16)
            # Central two bits.
            self._check_print_line(
                printer, 3 << (printer.print_height//2-1),
                b'\x00' * 7 + b'\x01\x80' + b'\x00' * 7)

    def test_print_bytes_from_int__24mm_tape(self):
        # 128 print head "dots" used.
        with self.get_printer() as printer:
            # monkey patch the tape width.
            printer.status._raw_data[STATUS_MEDIA_WIDTH] = 24
            mask = 2 ** printer.print_height - 1
            shift = (128 - printer.print_height) // 2

            # All zero bits.
            self._check_print_line(printer, 0, b'\x00' * 16)
            # All one bits.
            self._check_print_line(printer, -1, b'\xff' * 16)
            # Only "leftmost" bit set.
            self._check_print_line(printer, 1, b'\x00' * 15 + b'\x01')
            # Largest useful bit set.
            self._check_print_line(
                printer, 1 << (printer.PRINT_DOTS-1), b'\x80' + b'\x00' * 15)
            # Value is too big.
            self._check_print_line(
                printer, 1 << (printer.PRINT_DOTS), b'\x00' * 16)
            # Central two bits.
            self._check_print_line(
                printer, 3 << (printer.PRINT_DOTS//2-1),
                b'\x00' * 7 + b'\x01\x80' + b'\x00' * 7)

    def _make_test_image_sequence(self, printer, im_height, im_length):
        """Return some test images.

        Generates a simple test image of size im_length x im_height.
        im_height/2 slices will be yielded, each containing two raster lines
        of width `im_height`.
        """
        if printer.print_height < im_height:
            self.skipTest('Tape width is too narrow for this test.')
        full_image = Image.new('1', (im_length, im_height), color=1)
        # Draw black lines at the edges and a diagonal.
        draw = ImageDraw.Draw(full_image)
        draw.line(
            (0, 0,
             im_length-1, 0,
             im_length-1, im_height-1,
             0, im_height-1,
             0, 0,
             im_length-1, im_height-1),
            fill=0)
        # We want a sequence of images, so "slice" full_image
        # vertically.
        return (
            full_image.crop((offset, 0, offset+2, im_height))
            for offset in range(0, im_length, 2))

    def test_print_images_no_printer_errors(self):
        with self.get_printer() as printer:
            im_size = 26
            with DeviceLogger.patch(printer._dev) as logger:
                im_iter = self._make_test_image_sequence(
                    printer, im_size, im_size)
                printer.print_images(im_iter)

            def check_command(log_entry, cmd, kwargs=None):
                self.assertTrue(
                    log_entry[1].args[1].startswith(cmd),
                    f'Expected command {cmd} but found {log_entry[1].args[1]}')
                self.assertEqual(2, log_entry[1].args[0])

            write_cmds = list(logger.write_log_entries)
            check_command(write_cmds[0], CMD_INVALIDATE)
            check_command(write_cmds[1], CMD_INITIALIZE)
            check_command(write_cmds[2], CMD_STATUS_INFORMATION_REQUEST)
            check_command(write_cmds[3], CMD_SWITCH_TO_RASTER_MODE)
            check_command(write_cmds[4], CMD_COMPRESSION_MODE_TIFF)
            check_command(write_cmds[5], CMD_SET_MARGIN)
            for count in range(im_size):
                check_command(write_cmds[6+count], CMD_RASTER_GRAPHICS_TRANSFER)
                # XXX check the sent data too? It's possible to construct a
                # PIL Image from the raster data.
            check_command(write_cmds[6+im_size], CMD_PRINT_AND_FEED)
            # These were all sent commands-
            self.assertEqual(7+im_size, len(write_cmds))

            read_data = list(logger.read_log_entries)
            # We got four status messages. XXX The check for exactly
            # four log entries is too simple. It can happen that a call
            # of P700._dev.read() returns less than 32 bytes and that
            # another read() call is necessary to get the complete status
            # data.
            self.assertEqual(4, len(read_data))
            # The read data in all entries are status data, i.e. byte arrays
            # of length 32. XXX See above: Too simple.
            for entry in read_data:
                self.assertTrue, isinstance(entry[1].result, array)
                self.assertEqual('B', entry[1].result.typecode)
                self.assertEqual(32, len(entry[1].result))

            status = PrinterStatus(read_data[0][1].result)
            self.assertEqual(
                StatusReason.Reply_To_Status_Request, status.reason)
            self.assertEqual(PrintPhase.Editing, status.print_phase)

            status = PrinterStatus(read_data[1][1].result)
            self.assertEqual(StatusReason.Phase_Change, status.reason)
            self.assertEqual(PrintPhase.Printing, status.print_phase)

            status = PrinterStatus(read_data[2][1].result)
            self.assertEqual(StatusReason.Printing_Completed, status.reason)
            self.assertEqual(PrintPhase.Printing, status.print_phase)

            status = PrinterStatus(read_data[3][1].result)
            self.assertEqual(StatusReason.Phase_Change, status.reason)
            self.assertEqual(PrintPhase.Editing, status.print_phase)

    def test_print_images_cover_opened(self):
        # Like test_print_images_no_printer_errors() but force an error
        # by pausing the image generator so that the cover can be opened.
        with self.get_printer() as printer:
            im_size = 26
            with DeviceLogger.patch(printer._dev) as logger:
                def pausing_iterator():
                    for index, image in enumerate(
                            self._make_test_image_sequence(
                                printer, im_size, im_size)):
                        yield image
                        if index == 3:
                            input(
                                'Open the printer cover to force an error '
                                'then press the <return> key')
                with self.assertRaises(BLPPrintError) as ctx:
                    printer.print_images(pausing_iterator())
            # The printer is now in an error state.
            self.assertEqual(
                StatusReason.Error_Occurred, printer.status.reason)
            self.assertEqual(
                (Errors.Cover_Open, ), printer.status.errors)
            self.assertEqual(
                Notification.No_Notification, printer.status.notification)

            # Thats's the same status as seen when the exception was
            # detected. Since all status properties are generated from the
            # raw data, compare just this raw data.
            self.assertEqual(
                printer.status._raw_data, ctx.exception.args[1]._raw_data)
            # But it is not the same status instance.
            self.assertIsNot(printer.status, ctx.exception.args[1])
            # Instead, printer.status is an instance created by
            # P700._check_status() for another status message received after
            # the first one.
            self.assertIs(printer.status, ctx.exception.args[2][0])
            # Another error message was not received by _check_status()
            self.assertEqual(1, len(ctx.exception.args[2]))

            # For completeness, also check if there is any status message with
            # the notification "cover open". Such a notification is
            # mentioned on page 29 of the Raster Command Reference
            # of the PT-P700. But is does not occur anywhere...
            read_data = logger.read_log_entries
            for status_data in read_data:
                status = PrinterStatus(status_data[1].result)
                self.assertEqual(
                    Notification.No_Notification, status.notification)
            # Just for completeness: There is no additional message
            # pending. One second waiting time should be plenty for the
            # printer so send a notification "cover open".
            self.assertFalse(
                printer._update_status(0),
                f'Unexpected status message: {printer.status}')

            # Finally, let's see what the printer state is when the cover
            # is closed again.
            input('Close the printer cover again and press the <return> key')

            # The printer did not send any new status message.
            self.assertFalse(printer._update_status(0))
            # If the status is explicitly polled, the "open cover"
            # error is now gone.
            printer.read_status()
            # Sigh... We _sometimes_ get the error "no media" even if
            # tape is still present. Som well lets check for wto possible
            # results.
            #self.assertEqual((), printer.status.errors)
            self.assertIn(
                printer.status.errors,
                ((),
                 (Errors.No_Media, )))
            # But there is no notification again, despite such a thing
            # described on page 29 of the Raster Command Reference.
            self.assertEqual(
                Notification.No_Notification, printer.status.notification)
            # The printer is still in status "printing". So it would be
            # possible to continue.
            self.assertEqual(PrintPhase.Printing, printer.status.print_phase)

            # An init_device() call brings the P700 back into the status
            # "editing".
            printer.init_device()
            printer.read_status()
            self.assertEqual(PrintPhase.Editing, printer.status.print_phase)
            # Hrm. Sometimes we can get no error at all, sometimes the
            # printer reports ""no media".
            self.assertIn(printer.status.errors, ((), (Errors.No_Media, )))
            # No errors or notifications.
            self.assertEqual(
                Notification.No_Notification, printer.status.notification)

    @skipUnless(
        os.getenv('EXPENSIVE_TESTS'),
        'Set the environment variable EXPENSIVE_TESTS to run this test. '
        'Be aware that it consumes 1 meter tape.')
    def test_print_images_long_label(self):
        # Print a label of ca 100mm length, width small enough for 6mm tape.
        # Main purpose: Check that no timeout or some other error occurs.
        with self.get_printer() as printer:
            with DeviceLogger.patch(printer._dev) as logger:
                im_iter = self._make_test_image_sequence(
                    printer, 70, int(1000/25.4*180))
                printer.print_images(im_iter)
